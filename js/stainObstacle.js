StainObstacle = function (index, game, posx, posy) {
    if(GameConfig.debug) {
        console.log("create stain id" + index);
    }
    // Time of bonus activity
    this.timeOfBonus = 2;

    this.x = posx;
    this.y = posy;

    this.id = index;
    this.game = game;

    this.sprite = this.game.add.sprite(posx, posy, 'stain');
    this.sprite.anchor.set(0.5);

    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
};

StainObstacle.prototype.update = function () {
    let that = this;
    new Common().processTimeEntity(this, function () {
            let direction = Math.random() * 2 - 2;
            that.game.player.player.angle += direction * 4;
        },
        function () {
            globalSounds.plama_oleju.play();
            globalSounds.kroki.volume = 0;
        },
        function () {
            globalSounds.kroki.volume = 1;
        });
};
