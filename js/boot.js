var BootState = function (game) {
};

BootState.prototype = {
    common: null,

    preload: function () {
        this.common = new Common();
    },

    create: function () {
        //  We're going to be using physics, so enable the Arcade Physics system
        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.state.start('load');
    }
};