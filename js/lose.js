var LoseState = function (game) {
};

LoseState.prototype = {
    common: null,

    preload: function () {
        this.common = new Common();
    },

    create: function () {
        this.common.loadSplash('splash_lose');

        let that = this;
        let endGameScore = game.state.states['endGameScore'];
        let endGameTime = game.state.states['endGameTime'];
        let endGameMainScore = ((5 * endGameScore + 10) / (endGameTime + 1)) * 100;

        game.add.text(16, 16, 'You LOST :(\nyour points: ' + parseFloat(endGameScore).toFixed(0) + '\nyour time: ' + parseFloat(endGameTime).toFixed(2) + '\nyour SCORE: ' + parseFloat(endGameMainScore).toFixed(0), {
            fontSize: '32px',
            fill: '#fff',
            stroke: '#000000',
            strokeThickness: 3,
        });

        this.common.addMenuOption('FullScreen "F"', Phaser.Keyboard.F, function (e) {
            globalSounds.menu_click.play();
            that.common.toggleFullScreen();
        });
        this.common.addMenuOption('Main menu "M"', Phaser.Keyboard.M, function (e) {
            globalSounds.menu_click.play();
            that.menu();
        });
        this.common.addMenuOption('Restart "Spacebar"', Phaser.Keyboard.SPACEBAR, function (e) {
            globalSounds.menu_click.play();
            that.restart();
        });

        this.common.saveScore($.cookie('userName'), endGameMainScore, function(){
            that.common.showHiScores();
        });

        globalSounds.kroki_szybkie.stop();
        globalSounds.kroki.stop();
        globalSounds.main_theme.stop();
        globalSounds.przesuwanie_skrzyni.stop();
        globalSounds.przesuwanie_skrzyni.volume = 0;
        globalSounds.lose.play();
    },

    restart: function () {
        game.state.start('playMonster', true, false);
    },

    menu: function () {
        game.state.start('menu', true, false);
    }
};