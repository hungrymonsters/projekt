var MenuState = function (game) {
};

MenuState.prototype = {
    common: null,
    userName: null,
    defaultUserName: 'Master Gamer',

    preload: function () {
        this.common = new Common();
        game.add.plugin(PhaserInput.Plugin);
    },

    create: function () {
        this.common.loadSplash('splash_start');

        var that = this;

        game.add.text(16, 16, 'HunGRy Monster', {
            fontSize: '40px',
            fill: '#fff',
            stroke: '#000000',
            strokeThickness: 2,
        });

        this.common.addMenuOption('FullScreen "F"', Phaser.Keyboard.F, function (e) {
            globalSounds.menu_click.play();
            that.common.toggleFullScreen();
        });
        this.common.addMenuOption('Start "Spacebar"', Phaser.Keyboard.SPACEBAR, function (e) {
            globalSounds.menu_click.play();
            that.start();
        });

        //Input text field
        this.userName = game.add.inputField(16, 120, {
            font: '18px Arial',
            fill: '#212121',
            fontWeight: 'bold',
            width: 250,
            padding: 8,
            borderWidth: 1,
            borderColor: '#000',
            borderRadius: 6,
            placeHolder: 'Enter your name',
        });

        // get user name from cookie
        if ($.cookie('userName')) {
            this.userName.setText($.cookie('userName'));
        }

        this.common.showHiScores();

        globalSounds.menu_theme.play('', 0, 0.25, true, false);
    },

    start: function () {
        // set user name in cookie
        if (this.userName.value.length > 0) {
            $.cookie('userName', this.userName.value);
        }
        else {
            $.cookie('userName', this.defaultUserName);
        }

        globalSounds.menu_theme.volume = 0;
        globalSounds.menu_theme.stop();
        game.state.start('playMonster');
    }
};