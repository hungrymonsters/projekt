SlowDown = function (index, game, posx, posy, speed = 50) {
    if(GameConfig.debug) {
        console.log("create slowdown id" + index);
    }
    this.availableSprites = ['crucible', 'jira', 'email'];
    this.textInfo = null;

    // Time of bonus activity
    this.timeOfBonus = 3;
    this.bonusStart = null;

    this.x = posx;
    this.y = posy;

    this.id = index;
    this.game = game;

    this.chosenSprite = this.availableSprites[Math.floor(Math.random() * this.availableSprites.length)];
    this.sprite = this.game.add.sprite(posx, posy, this.chosenSprite);
    this.sprite.anchor.set(0.5);

    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);

    this.sprite.body.bounce.set(1);
    // this.sprite.body.immovable = true;
    this.sprite.body.velocity.set(0, speed);
};

SlowDown.prototype.update = function () {
    this.game.physics.arcade.collide(this.sprite, this.game.layer);
    let that = this;
    new Common().lockIfCovered(that, that.sprite, function () {
        new Common().processTimeEntity(that, function () {
            that.game.player.currentSpeed = 100;
            let playerPos = that.game.player.player.body.position;
            that.text(playerPos);
        },
        function () {
            globalSounds[that.chosenSprite].play();
        },
        function () {
            // Remove text
            if (that.bonusStart === null && that.sprite === null) {
                that.textInfo.destroy();
                that.textInfo = null;
            }
        });
    });
};

SlowDown.prototype.text = function (playerPos) {
    let info = "";
    if (this.chosenSprite === 'crucible') {
        info = "You've got code review to do first!";
    } else if (this.chosenSprite === 'jira') {
        info = "You've got new urgent task on Jira!";
    } else {
        info = 'Someone flooded our servers with spam.\n Servers are slowed down';
    }

    if (this.textInfo !== null) {
        this.textInfo.destroy();
        this.textInfo = null;
    }
    this.textInfo = this.game.add.text(playerPos.x - 100, playerPos.y - 70, info, {font: "20px Arial", fill: "#eee", stroke: '#000', strokeThickness: 1});
    this.textInfo.lineSpacing = 0;
};
