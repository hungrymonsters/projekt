var PlayMonsterState = function (game) {
    this.map = null;
    this.layer = null;
    this.player = null;
    this.ready = false;
    this.cursors = false;
    this.boxes = [];
    this.timeObstacles = [];
    this.sandwitches = [];
    this.deadWall = null;
	this.graphics;
    this.teleport = [];
    this.startCounter = null;
    this.winObject = null;
    this.keyB = null;
    this.keyV = null;
    this.lastPressKeyTime = new Date().getTime();
    this.debugPosCollection = [];
    this.kasztans = [];

    this.common = false;

    //scores
    this.score = 0;
    this.timer = 0;

    //textareas
    this.scoreText = null;
    this.timerText = null;

    this.intervalID = null;
};


PlayMonsterState.prototype = {
    init: function () {
        this.score = 0;
        this.scoreText = null;
        this.startCounter = null;
        this.ready = false;
        this.timer = 0;
        this.timerText = null;
    },

    preload: function () {
        this.common = new Common();
    },

    create: function () {
        this.common.addToggleFullScreenListner(Phaser.Keyboard.F);

        //start create world
        this.map = this.add.tilemap('map');
        this.map.addTilesetImage('floor_sprites', 'floor_sprites');

        this.layer = this.map.createLayer('Tile Layer 1');

        /* WALLs */
        this.map.setCollisionBetween(101, 200, true, this.layer);

        //test callback overide wall collision
        for (let i = 121; i <= 200; i++) { //@TODO  BUG !!!
            this.map.setTileIndexCallback(i, this.wallCollisionHandler, this);
        }

        //test add speed collision
        // this.map.setTileIndexCallback(34, this.speedCollisionHandler, this);

        //test cactus collision
        // this.map.setTileIndexCallback(31, this.cactusCollisionHandler, this);

        // Generate boxes
        var boxesList = game.cache.getJSON('boxesList');
        for(var key in boxesList){
            this.boxes[key] = new MovableBox(key, this, boxesList[key].posX, boxesList[key].posY);
        }
        this.boxes.sort(function (box1, box2) {
            return box1.x - box2.x;
        });

        var stainceList = game.cache.getJSON('stainceList');
        for(var key in stainceList) {
            this.timeObstacles.push(new StainObstacle(key, this, stainceList[key].posX, stainceList[key].posY));
        }

        var slowDownList = game.cache.getJSON('slowDownList');
        for(var key in slowDownList) {
            this.timeObstacles.push(new SlowDown(key, this, slowDownList[key].posX, slowDownList[key].posY));
        }

        var justiceList = game.cache.getJSON('justiceList');
        for(var key in justiceList) {
            this.timeObstacles.push(new JuiceBonus(key, this, justiceList[key].posX, justiceList[key].posY));
        }

        // Add sandwiches
        var sandwitchesList = game.cache.getJSON('sandwitchesList');
        for(var key in sandwitchesList) {
            this.sandwitches[key] = new CollectedSandwich(key, this, sandwitchesList[key].posX, sandwitchesList[key].posY);
        }

        this.boxes.forEach(function (box) {
            box.box.bringToTop();
        });

        // Add kasztans
        var kasztanList = game.cache.getJSON('kasztanList');
        for(var key in kasztanList) {
            this.kasztans[key] = new kasztanObject(this, kasztanList[key].posX, kasztanList[key].posY);
        }

        this.teleport.push(new Teleport(0, this, 700, 400, 900, 300));
        this.teleport.push(new Teleport(0, this, 4850, 150, 5000, 1100));
        this.deadWall = new deadWall(this);
        this.winObject = new winObject(this, 15815, 590);

        this.showScore(0);

        globalSounds.main_theme.play('', 0, 0.25, true, false);
        this.startCounter = new StartCounter(this, 300, 300);

        this.showTimer();

        this.createPlayer();
        this.player.player.bringToTop();
    },

    createPlayer: function () {
        this.player = new Player(1, this);
        this.cursors = this.input.keyboard.createCursorKeys();
        if(GameConfig.debug) {
            this.keyB = game.input.keyboard.addKey(Phaser.Keyboard.B);
            this.keyV = game.input.keyboard.addKey(Phaser.Keyboard.V);
        }
        this.world.setBounds(0, 0, GameConfig.worldSizeW, GameConfig.worldSizeH);
        this.camera.follow(this.player.player);
    },

	update: function () {
        if (this.startCounter.sprite !== null) {
            this.startCounter.update();
        }

        if (!this.ready) return;

		let that = this;
        this.player.input.left = this.cursors.left.isDown;
		this.player.input.right = this.cursors.right.isDown;
		this.player.input.up = this.cursors.up.isDown;
		this.player.input.down = this.cursors.down.isDown;
        // this.player.input.fire = game.input.activePointer.isDown;

        if(GameConfig.debug) {
            if((new Date().getTime() - this.lastPressKeyTime) > 500) {
                if(this.keyV.isDown) {
                    console.log(this.debugPosCollection);
                    console.log(JSON.stringify(this.debugPosCollection));
                    this.lastPressKeyTime = new Date().getTime();
                }
                if(this.keyB.isDown) {
                    console.log( );
                    this.debugPosCollection.push(
                        {
                            'posX' : this.player.player.x,
                            'posY' : this.player.player.y
                        }
                    );
                    this.lastPressKeyTime = new Date().getTime();
                }
            }
        }

        this.teleport.forEach(function (teleport) {
            teleport.update();
        });

        this.boxes.forEach(function (box) {
            box.update();
        });
        this.timeObstacles.forEach(function (obstacle) {
            obstacle.update();
        });
        // Leave bonuses either not acquired or still active
        this.timeObstacles = this.timeObstacles.filter(function (obstacle) {
            return !(obstacle.bonusStart === null && obstacle.sprite === null);
        });

        this.sandwitches.forEach(function (sandwitch) {
            sandwitch.update(function(){
                //collback collect sandwich
                that.score += GameConfig.scoreForSandwich;
                that.showScore(that.score);
            });
        });

        for(var key in this.kasztans) {
            this.kasztans[key].update();
        }

        this.player.update();
        this.deadWall.update();
        this.winObject.update();
    },

    wallCollisionHandler: function (sprite, tile) {
        // tile.alpha = 0.2;

        this.player.wallCollision();
        this.layer.dirty = true;

        return true;

    },

    speedCollisionHandler: function (sprite, tile) {
        // console.log('collision detect');
        // tile.alpha = 0.2;

        this.player.speedCollision();
        // this.layer.dirty = true;

        return true;

    },

    cactusCollisionHandler: function (sprite, tile) {
        this.player.cactusCollision();

        return true;

    },

    render: function () {
        if(GameConfig.debug) {
            //    this.game.debug.cameraInfo(this.game.camera, 32, 32);
            this.game.debug.spriteInfo(this.player.player);
        }
    },

    showScore: function (points) {
        if (this.scoreText === null) {
            this.scoreText = game.add.text(GameConfig.width - 180, 10, 'Score: ' + points, {
                fontSize: '32px',
                fill: '#fff',
                stroke: '#000000',
                strokeThickness: 2,
            });
            this.scoreText.fixedToCamera = true;
        } else {
            this.scoreText.text = 'Score: ' + points;
        }
    },

    showTimer: function () {
        let that = this;

        this.intervalID = setInterval(function () {
            if (that.timerText === null) {
                that.timerText = game.add.text(10, 10, 'Time: ' + parseFloat(that.timer).toFixed(2), {
                    fontSize: '32px',
                    fill: '#fff',
                    stroke: '#000000',
                    strokeThickness: 2,
                });
                that.timerText.fixedToCamera = true;
            } else {
                that.timerText.text = 'Time: ' + parseFloat(that.timer).toFixed(2);
            }

            if(that.ready !== false) {
                that.timer = that.timer + 0.1;
            }
        }, 100);
    },

    win: function () {
        game.state.states['endGameScore'] = this.score;
        game.state.states['endGameTime'] = this.timer;

        game.state.start('win', true, false);
        clearInterval(this.intervalID);
    },

    lose: function () {
        game.state.states['endGameScore'] = this.score;
        game.state.states['endGameTime'] = this.timer;

        game.state.start('lose', true, false);
        clearInterval(this.intervalID);
    }
}