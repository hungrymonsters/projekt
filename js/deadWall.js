deadWall = function (game) {
    //this.x = game.player.player.x - 100 - GameConfig.deadWallW;
    //this.y = game.player.player.y - (GameConfig.deadWallH / 2);
    this.game = game;
    // this.graphics = this.game.add.graphics(0, 0);
    this.wall = this.game.add.sprite(-GameConfig.deadWallW+80, 0, 'wall');

    this.getX = function(){
        return this.wall.x + GameConfig.deadWallW;
    }
    this.game.physics.enable(this.wall, Phaser.Physics.ARCADE);
    this.currSpeed = 0;

};

deadWall.prototype.update = function () {
    // console.log(this.x);
    if(this.wall.body.velocity.x != 0 || this.game.player.player.body.velocity.x != 0) {
        this.wall.body.velocity.x = Math.max(150, Math.min(this.game.player.player.body.velocity.x - 100, GameConfig.playerMaxSpeed) * 0.85);
    }

    if (this.wall.x + 1200 < this.game.player.player.x) {
        this.wall.x += 100;
    }
    /**
    this.currSpeed += 5;
    if(this.currSpeed > GameConfig.dealWallMinSpeed) {
        this.currSpeed = GameConfig.dealWallMinSpeed;
    }

    if(this.game.player.currentSpeed > GameConfig.playerMaxSpeed) {
        this.wall.body.velocity.x = (this.game.player.currentSpeed * 0.9);
    }
    this.wall.body.velocity.x = this.currSpeed;
    **/
    if(this.getX() > this.game.player.player.x) {
        this.game.lose();
    }

    // this.game.physics.arcade.collide(this.game.player.player, this.wall, this.stopMove, null, this);
};
