MovableBox = function (index, game, posx, posy, weight = 1000) {
    if(GameConfig.debug) {
        console.log("create box id" + index);
    }
    this.x = posx;
    this.y = posy;

    this.id = index;
    this.game = game;

    this.box = this.game.add.sprite(posx, posy, 'box');
    this.box.anchor.set(0.5);

    this.game.physics.enable(this.box, Phaser.Physics.ARCADE);

    this.box.body.mass = weight;
    this.box.body.drag.set(weight);
};

MovableBox.prototype.update = function () {
    let that = this;
    this.game.boxes.forEach(function (box) {
        if (box.id !== that.id) {
            that.game.physics.arcade.collide(that.box, box.box);
        }
    });
    this.game.physics.arcade.collide(this.box, this.game.player.player, function(box, player){
        if (box.body.velocity.x > 0 || box.body.velocity.y > 0) {
            globalSounds.przesuwanie_skrzyni.play('', 0, 0.25, true, false);
            globalSounds.przesuwanie_skrzyni.volume = 0.25;
        }
        else{
            globalSounds.przesuwanie_skrzyni.volume = 0;
            globalSounds.przesuwanie_skrzyni.stop();
        }
    });
    this.game.physics.arcade.collide(this.box, this.game.layer);
};
