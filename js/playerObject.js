Player = function (index, game) {
    if(GameConfig.debug) {
        console.log("create player id" + index)
    }
    this.cursor = {
        left: false,
        right: false,
        up: false,
        down: false,
        fire: false
    };

    this.input = {
        left: false,
        right: false,
        up: false,
        down: false,
        fire: false
    };

    this.x = 0;
    this.y = 0;

    this.id = index;
    this.game = game;
    this.health = 30;
    this.currentSpeed = 0;

    this.player = this.game.add.sprite(150, 325, 'player');
    this.player.anchor.set(0.5);
    game.physics.enable(this.player, Phaser.Physics.ARCADE);
    this.player.animations.add('walk', [0, 1, 2, 1, 0, 3, 4, 3, 0], 10, true);
    this.player.animations.add('wait', [0], 10, false);

    this.player.angle = 0;
    this.player.body.collideWorldBounds = true;
    this.player.body.drag.set(100);
};

Player.prototype.update = function () {
    this.game.physics.arcade.collide(this.player, this.game.layer);

    //fizyka "sterowania" graczem
    // console.log(this.cursor);
    if (this.input.left) {
        this.player.angle -= 5;
    }
    else if (this.input.right) {
        this.player.angle += 5;
    }

    if (this.input.up) {
        //  The speed we'll travel at
        this.currentSpeed += 5;
        if (this.currentSpeed > GameConfig.playerMaxSpeed) {
            this.currentSpeed *= 0.8;
        }

        if (this.currentSpeed > GameConfig.playerSpeedBustMax) {
            this.currentSpeed = 500;
        }

        this.player.animations.play('walk');
    }
    else {
        if (this.currentSpeed > 0) {
            this.currentSpeed -= 5;
        }
    }

    if (this.currentSpeed > 0) {
        this.game.physics.arcade.velocityFromRotation(this.player.rotation, this.currentSpeed, this.player.body.velocity);

        globalSounds.kroki.play('', 0, 1, true, false);
    }
    else {
        this.game.physics.arcade.velocityFromRotation(this.player.rotation, 0, this.player.body.velocity);
        this.player.animations.play('wait');

        globalSounds.kroki.stop();
    }

};
