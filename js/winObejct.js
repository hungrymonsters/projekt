winObject = function (game, posx, posy) {
    if(GameConfig.debug) {
        console.log("win object id");
    }
    this.x = posx;
    this.y = posy;
    this.game = game;

    this.sprite = this.game.add.sprite(posx, posy, 'win');
    this.sprite.anchor.set(0.5);

    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
};

winObject.prototype.update = function () {
    let that = this;

    this.game.physics.arcade.collide(this.sprite, this.game.player.player, function (object, player) {
        that.game.win();
    });
};
