StartCounter = function (game, posx, posy) {
    this.countFrom = 4;
    this.startTime = new Date().getTime();

    this.x = posx;
    this.y = posy;

    this.game = game;
    this.text = null;

    let width = GameConfig.width * 2;
    let height = GameConfig.height * 2;

    let bitmap = this.game.add.bitmapData(width, height);
    bitmap.ctx.beginPath();
    bitmap.ctx.rect(0, 0, width, height);
    bitmap.ctx.fillStyle = '#00000080';
    bitmap.ctx.fill();
    this.sprite = game.add.sprite(width, height, bitmap);
    this.sprite.anchor.setTo(1);
    globalSounds.go321.play();
};

StartCounter.prototype.update = function () {
    if (this.text !== null) {
        this.text.destroy();
    }
    let elapsed = Math.floor((new Date().getTime() - this.startTime) / 900);
    let remaining = this.countFrom - 1 - elapsed;
    this.text = this.game.add.text(GameConfig.width / 2, GameConfig.height / 2, remaining, {font: "90px Arial", fill: "#ccc", stroke: '#000', strokeThickness: 3});

    if (this.countFrom - elapsed <= 0) {
        this.sprite.destroy();
        this.text.destroy();
        this.sprite = this.text = null;
        this.game.ready = true;
    }
};
