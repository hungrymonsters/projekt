JuiceBonus = function (index, game, posx, posy) {
    // Time of bonus activity
    this.timeOfBonus = 2;
    if(GameConfig.debug) {
        console.log("create juice id" + index);
    }
    this.bonusStart = null;

    this.x = posx;
    this.y = posy;

    this.id = index;
    this.game = game;

    this.sprite = this.game.add.sprite(posx, posy, 'juice');
    this.sprite.anchor.set(0.5);

    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
};

JuiceBonus.prototype.update = function () {
    let that = this;
    new Common().lockIfCovered(that, that.sprite, function () {
        new Common().processTimeEntity(that, function () {
            that.game.player.currentSpeed = GameConfig.playerSpeedBustMax + 1;
        },
        function () {
            globalSounds.zebranie_soczku.play();
            globalSounds.kroki_szybkie.play('', 0, 1, true, false);
            globalSounds.kroki.volume = 0;
        },
        function () {
            globalSounds.kroki.volume = 1;
            globalSounds.kroki_szybkie.volume = 0;
            globalSounds.kroki_szybkie.stop();
            globalSounds.koniec_soczku.play();
        });
    });
};
