// Initialize Phaser, and create a 800px by 600px game
game = new Phaser.Game(GameConfig.width, GameConfig.height, Phaser.AUTO, 'gameDiv');

globalSounds = null;

// Add the 'mainState' and call it 'main'
game.state.add('boot', BootState);
game.state.add('load', LoadState);
game.state.add('menu', MenuState);
game.state.add('playMonster', PlayMonsterState);
game.state.add('win', WinState);
game.state.add('lose', LoseState);

// Start the state to actually start the game
game.state.start('boot');

//
var screen_change_events = "webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange";
$(document).on(screen_change_events, function () {
    let common = new Common();
    common.resizeGame();
});