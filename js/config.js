GameConfig = {
    width: 960,
    height: 540,

    restForceNode: true,
    restNodeEndpoint: window.location.protocol + "//" + window.location.hostname + ":3000/",
    restPhpEndpoint: window.location.href + 'server.php/',

    playerMaxSpeed: 500,
    playerSpeedBustMax: 850,
    maxWallCollisonSpeed: 800,

    // dealWallMinSpeed: 320,
    dealWallMinSpeed: 20, //only for dev

    worldSizeW: 16000,
    worldSizeH: 1280,
    deadWallW: 600,
    deadWallH: 1280,

    debug: false,

    scoreForSandwich: 20,
    scoreWinMultiplay: 5,
};