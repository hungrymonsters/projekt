Common = function () {
};

Common.prototype = {

    optionCount: 1,

    /**
     * Add menu option
     * @param text
     * @param letter eg.: Phaser.Keyboard.SPACEBAR
     * @param callback function
     */
    addMenuOption: function (text, letter, callback) {
        let optionStyle = {fontSize: '32px', fill: '#fff'};
        let txt = game.add.text(16, GameConfig.height - (16 + 32) * this.optionCount, text, optionStyle);

        txt.stroke = '#000000';
        txt.strokeThickness = 3;
        let onOver = function (target) {
            target.fill = "#FEFFD5";
            txt.useHandCursor = true;
        };
        let onOut = function (target) {
            target.fill = "white";
            txt.useHandCursor = false;
        };
        //txt.useHandCursor = true;
        txt.inputEnabled = true;
        txt.events.onInputUp.add(callback, this);
        txt.events.onInputOver.add(onOver, this);
        txt.events.onInputOut.add(onOut, this);

        //keyboard shortcut
        game.input.keyboard.addKey(letter).onDown.add(callback, this);

        this.optionCount++;
    },

    /**
     * Save user score and show hiscore
     * @param userName
     * @param score
     * @param callbackDone
     */
    saveScore(userName, score, callbackDone) {
        let scoreToSave = parseFloat(score).toFixed(0);

        $.post(this.getRestEndpoint() + 'users', {name: userName, score: scoreToSave})
            .done(function (data) {
                if (typeof callbackDone === 'function') {
                    callbackDone();
                }
            });
    },

    /**
     * Print hiscores
     */
    showHiScores() {
        $.get(this.getRestEndpoint() + 'users')
            .done(function (data) {
                let textGroup = game.add.group();

                textGroup.add(game.make.text(GameConfig.width - 250, 20, 'Top HiScores:', {
                    font: "28px Arial",
                    fill: "white",
                    stroke: '#000000',
                    strokeThickness: 2,
                }));
                for (let i = 0; i < data.length; i++) {
                    textGroup.add(game.make.text(GameConfig.width - 250, 60 + i * 26, data[i].score + ' - ' + data[i].name, {
                        font: "20px Arial",
                        fill: "white",
                        stroke: '#000000',
                        strokeThickness: 2,
                    }));
                }
            });
    },

    getRestEndpoint() {
        if ('localhost' === window.location.hostname || GameConfig.restForceNode) {
            return GameConfig.restNodeEndpoint;
        }
        else {
            return GameConfig.restPhpEndpoint;
        }
    },

    toggleFullScreen: function () {
        let that = this;

        //Toggle browser full screen mode
        if (game.state.states['fullScreen']) {
            game.scale.stopFullScreen();
        }
        else {
            game.scale.startFullScreen();
        }

        //timeout is necessary to reload browser state
        setTimeout(function () {
            that.resizeGame();

            //toggle state
            game.state.states['fullScreen'] = true !== game.state.states['fullScreen'];
        }, 500);
    },

    addToggleFullScreenListner: function (key) {
        let that = this;
        game.input.keyboard.addKey(key).onDown.add(function () {
            that.toggleFullScreen();
        }, this);
    },

    resizeGame: function () {
        if (game.state.states['fullScreen']) {
            game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
            game.scale.minWidth = GameConfig.width;
            game.scale.minHeight = GameConfig.height;
            game.scale.refresh();
        } else {
            let normalAspectRetio = GameConfig.width / GameConfig.height;
            let scaledAspectRetio = window.innerWidth / window.innerHeight;
            let width = window.innerWidth;
            let height = window.innerHeight;

            //przeliczenie rozciagniecia ekrany w celu unikniecie rozciagania gry (zachowuje proporcje przy fullscreanie)
            if (scaledAspectRetio > normalAspectRetio) {
                width = (width * normalAspectRetio) / scaledAspectRetio;
            }
            else {
                height = (height * scaledAspectRetio) / normalAspectRetio;
            }

            game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
            game.scale.minWidth = width;
            game.scale.minHeight = height;
            game.scale.refresh();
        }
    },

    /**
     * Przetwarzanie bonusu/znikajacej przeszkody
     * Callback jest wykonywany dopoki czas dzialania nie minal
     * @param that $this okreslonej encji
     * @param callback wykonywany dopoki bonus dziala
     * @param callbackSoundStart zagraj dzwiek jak zbierzesz powerup
     * @param callbackSoundEnd przerwij granie ciaglego dzwieku
     */
    processTimeEntity: function (that, callback, callbackSoundStart, callbackSoundEnd) {
        if (that.sprite === null) {
            let elapsedSinceStart = new Date().getTime() - that.bonusStart;
            if (elapsedSinceStart <= that.timeOfBonus * 1000) {
                callback();
            } else {
                // Bonus ended, mark it for removal
                that.bonusStart = null;
                callbackSoundEnd();
            }
            return;
        }
        that.game.physics.arcade.collide(that.sprite, that.game.player.player, function (sprite, player) {
            // Save time of activation
            that.bonusStart = new Date().getTime();
            sprite.destroy();
            that.sprite = null;
            callbackSoundStart();
        });
    },

    loadSplash: function (imageName) {
        game.stage.backgroundColor = "#03b4f2";
        game.add.sprite(300, 150, imageName);
    },

    lockIfCovered: function (that, sprite, callback) {
        let anyBoxOver = that.game.boxes.find(function (box) {
            if (box === undefined || sprite === null) return false;
            try {
                return Phaser.Rectangle.intersects(box.box.getBounds(), sprite.getBounds());
            } catch (e) {}
        });
        if (anyBoxOver === undefined) {
            callback();
        }
    }
};