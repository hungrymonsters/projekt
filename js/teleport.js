Teleport = function (index, game, posx, posy, targetx, targety) {
    if(GameConfig.debug) {
        console.log("create teleport id" + index);
    }
    this.x = posx;
    this.y = posy;

    this.target = {x: targetx, y: targety};

    this.id = index;
    this.game = game;

    this.sprite = this.game.add.sprite(posx, posy, 'portal');
    this.spriteOut = this.game.add.sprite(targetx, targety, 'portal-out');
    this.sprite.anchor.set(0.5);
    this.spriteOut.anchor.set(0.5);

    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);

    this.sprite.body.immovable = true;
};

Teleport.prototype.update = function () {
    let that = this;
    this.game.physics.arcade.overlap(this.sprite, this.game.player.player, function (teleport, player) {
        new Common().lockIfCovered(that, that.sprite, function () {
            that.game.add
                .tween(that.game.player.player)
                .to({ alpha: 0 }, 300, Phaser.Easing.Linear.None, true, 0, 1, true);
            player.body.position.set(that.target.x, that.target.y);
            globalSounds.teleport.play();
        });
    });

};
//podpiac dzwiek zderzenia ze sciana