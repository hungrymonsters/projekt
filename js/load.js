var LoadState = function (game) {
};

LoadState.prototype = {
    preload: function () {
        game.add.text(16, game.world.height / 2 - 32, 'Loading ...', {fontSize: '32px', fill: '#fff'});

        game.load.tilemap('map', 'assets/maps/map.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.json('sandwitchesList', 'assets/maps/sandwitches.json');
        game.load.json('boxesList', 'assets/maps/boxes.json');
        game.load.json('stainceList', 'assets/maps/staince.json');
        game.load.json('slowDownList', 'assets/maps/slowDown.json');
        game.load.json('justiceList', 'assets/maps/justice.json');
        game.load.json('kasztanList', 'assets/maps/kasztan.json');

		game.load.image('floor_sprites', 'assets/maps/floor_sprites.png');
        game.load.spritesheet('player', 'assets/images/monster_keyframes_final.png', 60, 60);
		game.load.image('box', 'assets/images/box.png');
        game.load.image('juice', 'assets/images/juice.png');
        game.load.image('sandwich', 'assets/images/sandwich.png');
        game.load.image('box', 'assets/images/box.png');
        game.load.image('rotaryDoor', 'assets/images/rotaryDoor.png');
        game.load.image('wall', 'assets/images/wall.png', 600, 1280);
		game.load.image('stain', 'assets/images/stain.png');
		game.load.image('portal', 'assets/images/portal-in.png');
		game.load.image('portal-out', 'assets/images/portal-out.png');
		game.load.image('crucible', 'assets/images/crucible.png');
		game.load.image('email', 'assets/images/email.png');
		game.load.image('jira', 'assets/images/jira.png');
        game.load.image('win', 'assets/images/win.png');
        game.load.image('kasztan', 'assets/images/kasztan.png');

		//splash
        game.load.image('splash_win', 'assets/images/splash_win.png');
        game.load.image('splash_lose', 'assets/images/splash_lose.png');
        game.load.image('splash_start', 'assets/images/splash_start.png');

        //sounds
        game.load.audio('main_theme', 'assets/sounds/music_game.mp3');
        game.load.audio('menu_theme', 'assets/sounds/music_menu_copy.mp3');
        game.load.audio('kroki', 'assets/sounds/kroki.mp3');
        game.load.audio('krzyk', 'assets/sounds/krzyk.wav');
        game.load.audio('otwarcie_drzwi', 'assets/sounds/otwarcie_drzwi.mp3');
        game.load.audio('pieniazek', 'assets/sounds/pieniazek.wav');
        game.load.audio('pojawienie_kanapki', 'assets/sounds/pojawienie_kanapki.wav');
        game.load.audio('speed', 'assets/sounds/speed.wav');
        game.load.audio('speed_2', 'assets/sounds/speed_2.wav');
        game.load.audio('wejscie_w_sciane', 'assets/sounds/wejscie_w_sciane.wav');
        game.load.audio('wejscie_w_sciane_2', 'assets/sounds/wejscie_w_sciane_2.wav');
        game.load.audio('win_stage', 'assets/sounds/win_stage_2.mp3');
        game.load.audio('zamkniete_drzwi', 'assets/sounds/zamkniete_drzwi.wav');
        game.load.audio('zebranie_kanapki', 'assets/sounds/zebranie_kanapki.wav');
        game.load.audio('zebranie_kanapki_2', 'assets/sounds/zebranie_kanapki_2.wav');
        game.load.audio('zebranie_soczku', 'assets/sounds/zebranie_soczku_2.mp3');
        game.load.audio('zegar', 'assets/sounds/zegar.mp3');
        game.load.audio('plama_oleju', 'assets/sounds/plama_oleju.mp3');
        game.load.audio('przesuwanie_skrzyni', 'assets/sounds/przesuwanie_skrzyni.mp3');
        game.load.audio('menu_click', 'assets/sounds/menu_click.wav');
        game.load.audio('lose', 'assets/sounds/lose.mp3');
        game.load.audio('win', 'assets/sounds/win.mp3');
        game.load.audio('kroki_szybkie', 'assets/sounds/kroki_szybkie.mp3');
        game.load.audio('teleport', 'assets/sounds/teleport.mp3');
        game.load.audio('koniec_soczku', 'assets/sounds/koniec_soczku.wav');
        game.load.audio('321go', 'assets/sounds/321go.wav');
        game.load.audio('email', 'assets/sounds/mail.mp3');
        game.load.audio('jira', 'assets/sounds/jira.mp3');
        game.load.audio('crucible', 'assets/sounds/crucible.mp3');
        game.load.audio('kasztanAudio', 'assets/sounds/kaktus_auu.mp3');
    },

    create: function () {

        //inicjalizacja dzwiekow do globalnego obiektu po zaladowaniu
        globalSounds = new GlobalSounds(game);

        game.state.start('menu');
    }
};