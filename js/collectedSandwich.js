CollectedSandwich = function (index, game, posx, posy) {

    if(GameConfig.debug) {
        console.log("create sandwich" + index);
    }
    this.x = posx;
    this.y = posy;

    this.id = index;
    this.game = game;

    this.sandwich = this.game.add.sprite(posx, posy, 'sandwich');
    this.sandwich.anchor.set(0.5);

    this.game.physics.enable(this.sandwich, Phaser.Physics.ARCADE);
};

CollectedSandwich.prototype.update = function (collback) {
    let that = this;

    this.game.physics.arcade.collide(this.sandwich, this.game.player.player, function (sandwich, player) {
        // Save time of activation
        that.bonusStart = new Date().getTime();
        sandwich.destroy();
        that.sandwich = null;

        globalSounds.zebranie_kanapki.play();

        collback();
    });

    this.game.physics.arcade.collide(this.sandwich, this.game.layer);
};