GlobalSounds = function (game) {
    this.kroki = game.add.audio('kroki');
    this.main_theme = game.add.audio('main_theme');
    this.zebranie_kanapki = game.add.audio('zebranie_kanapki');
    this.plama_oleju = game.add.audio('plama_oleju');
    this.zebranie_soczku = game.add.audio('zebranie_soczku');
    this.przesuwanie_skrzyni = game.add.audio('przesuwanie_skrzyni');
    this.menu_click = game.add.audio('menu_click');
    this.win_stage = game.add.audio('win_stage');
    this.lose = game.add.audio('lose');
    this.win = game.add.audio('win');
    this.kroki_szybkie = game.add.audio('kroki_szybkie');
    this.teleport = game.add.audio('teleport');
    this.koniec_soczku = game.add.audio('koniec_soczku');
    this.kaktus_auu = game.add.audio('kaktus_auu');
    this.menu_theme = game.add.audio('menu_theme');
    this.jira = game.add.audio('jira');
    this.email = game.add.audio('email');
    this.crucible = game.add.audio('crucible');
    this.go321 = game.add.audio('321go');
    this.kasztanAudio = game.add.audio('kasztanAudio');
};

GlobalSounds.prototype = {
};