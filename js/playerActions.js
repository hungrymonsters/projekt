
Player.prototype.wallCollision = function () {
    this.currentSpeed -= 10;
};

Player.prototype.speedCollision = function () {
    this.currentSpeed *= 2;
    if (this.currentSpeed > GameConfig.maxWallCollisonSpeed) {
        this.currentSpeed = GameConfig.maxWallCollisonSpeed;
    }
};

Player.prototype.cactusCollision = function () {
    this.player.angle += 180;
    if (this.currentSpeed < GameConfig.playerSpeedBustMax) {
        this.currentSpeed *= 3;
    }
    if (this.currentSpeed > GameConfig.maxWallCollisonSpeed) {
        this.currentSpeed = GameConfig.maxWallCollisonSpeed;
    }
};